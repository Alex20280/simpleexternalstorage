package layouts.sourceit.com.simpleexternalstorage;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static layouts.sourceit.com.simpleexternalstorage.Utils.isExternalStorageWritable;

public class MainActivity extends AppCompatActivity {
    //Only for API21 and below.

    public static final String DIR_NAME = "testDir5";
    public static final String FILE_NAME = "myFile11.txt";

    @BindView(R.id.am_login)
    EditText login;
    @BindView(R.id.am_pass)
    EditText pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        loadIfExists();
    }
    private void loadIfExists() {
        File sdCard = Environment.getExternalStorageDirectory(); // create file with contains path to external storage
        File directory = new File(sdCard, DIR_NAME); // create file with contains path to MyDir folder
        File file = new File(directory, FILE_NAME); // create file with contains path to myFile.txt file
        if (file.exists()) {
            //read file
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                login.setText(br.readLine());
                pass.setText(br.readLine());
                br.close(); //important: close reader
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @OnClick(R.id.am_action_save)
    void onSaveClick() {
        if (isExternalStorageWritable()) {
            //create dirs
            File sdCard = Environment.getExternalStorageDirectory();
            File directory = new File (sdCard, DIR_NAME);
            directory.mkdirs();

            //create file
            File file = new File(directory, FILE_NAME);

            try {
                //write data to file
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(fOut);
                osw.write(login.getText().toString());
                osw.write("\n");
                osw.write(pass.getText().toString());
                osw.flush();
                osw.close();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }

        }
    }
}
